package com.example.rany.servercommunicationretrofit.response.create_maincate;

import com.google.gson.annotations.SerializedName;

public class CreateMainCateResponse {
    @SerializedName("data")
    private Maincate maincate;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private boolean status;
    @SerializedName("code")
    private String code;

    public Maincate getMaincate() {
        return maincate;
    }

    public void setMaincate(Maincate maincate) {
        this.maincate = maincate;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
