package com.example.rany.servercommunicationretrofit.api_service;

import com.example.rany.servercommunicationretrofit.request.MainCateForm;
import com.example.rany.servercommunicationretrofit.response.UploadImageResponse;
import com.example.rany.servercommunicationretrofit.response.create_maincate.CreateMainCateResponse;
import com.example.rany.servercommunicationretrofit.response.maincate.MainCateResponse;
import com.example.rany.servercommunicationretrofit.response.sub_cate.SubCateResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface KnongDaiService {

    @GET("categories")
    Call<MainCateResponse> getAllMainCate();

    @GET("categories/sub-by-main-id/{id}")
    Call<SubCateResponse> getSubCateByMainCateId(@Path("id") int mainCateId );

    @Multipart
    @POST("urls/upload/web-icon")
    Call<UploadImageResponse> getImageString(@Part MultipartBody.Part photo);

    @POST("categories/create-main")
    Call<CreateMainCateResponse> insertNewCategory(@Body MainCateForm form);

}
