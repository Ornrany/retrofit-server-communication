package com.example.rany.servercommunicationretrofit.response.create_maincate;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Maincate {
    @SerializedName("icon_name")
    private String iconName;
    @SerializedName("des")
    private String des;
    @SerializedName("cate_name")
    private String cateName;
    @SerializedName("keywords")
    private List<Keywords> keywords;
    @SerializedName("status")
    private boolean status;
    @SerializedName("id")
    private int id;

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public List<Keywords> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keywords> keywords) {
        this.keywords = keywords;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
