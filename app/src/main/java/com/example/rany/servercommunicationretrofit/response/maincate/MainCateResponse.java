package com.example.rany.servercommunicationretrofit.response.maincate;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainCateResponse {
    @SerializedName("data")
    private List<Maincates> maincates;
    @SerializedName("msg")
    private String msg;
    @SerializedName("status")
    private boolean status;
    @SerializedName("code")
    private String code;

    public List<Maincates> getMaincates() {
        return maincates;
    }

    public void setMaincates(List<Maincates> maincates) {
        this.maincates = maincates;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
