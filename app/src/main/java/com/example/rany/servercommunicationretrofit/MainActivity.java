package com.example.rany.servercommunicationretrofit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.rany.servercommunicationretrofit.api_service.KnongDaiService;
import com.example.rany.servercommunicationretrofit.response.maincate.MainCateResponse;
import com.example.rany.servercommunicationretrofit.response.maincate.Maincates;
import com.example.rany.servercommunicationretrofit.response.sub_cate.Data;
import com.example.rany.servercommunicationretrofit.response.sub_cate.SubCateResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button btnGetMainCate;
    private Retrofit retrofit;
    private KnongDaiService service;
    public static final String TAG = "ooooo";
    private ListView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        setUpRetrofit();
        initEvent();
    }

    private void initEvent() {
        btnGetMainCate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllMainCate();
            }
        });
    }

    private void getAllMainCate(){
        Call<MainCateResponse> call = service.getAllMainCate();
        call.enqueue(new Callback<MainCateResponse>() {
            @Override
            public void onResponse(Call<MainCateResponse> call, Response<MainCateResponse> response) {
                Log.e(TAG, response.body().getMsg() );
                // get response date -> list maincates
                List<Maincates> maincatesList = response.body().getMaincates();

                // get all maincates's name
                List<String> name = new ArrayList<>();

                // get all maincates's id -> parse to method getSubCateByMainCateId;
                final List<Integer> mainCateId = new ArrayList<>();
                for(Maincates maincates : maincatesList){
                    name.add(maincates.getCateName());
                    mainCateId.add(maincates.getId());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                      MainActivity.this,
                      android.R.layout.simple_list_item_1,
                        name
                );
                view.setAdapter(adapter);
                view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        getSubCateByMainCateId(mainCateId.get(position));
                    }
                });
                //Log.e(TAG, "onResponse: "+ name.toString() );
            }

            @Override
            public void onFailure(Call<MainCateResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage() );
            }
        });
    }

    private void getSubCateByMainCateId(int id){
        Call<SubCateResponse> call = service.getSubCateByMainCateId(id);
        call.enqueue(new Callback<SubCateResponse>() {
            @Override
            public void onResponse(Call<SubCateResponse> call, Response<SubCateResponse> response) {
//                Log.e(TAG, "onResponse: "+ response.body().getMsg() );
                List<Data> subCateList = response.body().getData();
                List<String> subCateName = new ArrayList<>();
                for(Data subCate : subCateList){
                    subCateName.add(subCate.getCateName());
                }
                Log.e(TAG, "onResponse: "+ subCateName.toString() );
            }

            @Override
            public void onFailure(Call<SubCateResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ t.getMessage() );
            }
        });
    }

    private void setUpRetrofit() {
         retrofit = new Retrofit.Builder()
                .baseUrl("http://110.74.194.125:15000/api/v1/")
                 .addConverterFactory(GsonConverterFactory.create())
                .build();

         service = retrofit.create(KnongDaiService.class);
    }

    private void initView() {
        btnGetMainCate = findViewById(R.id.btnGetMainCategories);
        view = findViewById(R.id.lvMainCate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itmAdd:
                startActivity(new Intent(this, AddMainCate.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
