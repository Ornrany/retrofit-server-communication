package com.example.rany.servercommunicationretrofit.response.create_maincate;

import com.google.gson.annotations.SerializedName;

public class Keywords {
    @SerializedName("keyword_name")
    private String keywordName;
    @SerializedName("status")
    private boolean status;
    @SerializedName("id")
    private int id;

    public String getKeywordName() {
        return keywordName;
    }

    public void setKeywordName(String keywordName) {
        this.keywordName = keywordName;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
