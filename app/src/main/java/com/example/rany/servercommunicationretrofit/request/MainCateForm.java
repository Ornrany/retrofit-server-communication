package com.example.rany.servercommunicationretrofit.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainCateForm {
    @SerializedName("status")
    private boolean status;
    @SerializedName("keywords")
    private List<String> keywords;
    @SerializedName("id")
    private int id;
    @SerializedName("icon_name")
    private String iconName;
    @SerializedName("des")
    private String des;
    @SerializedName("cate_name")
    private String cateName;

    public MainCateForm(boolean status, List<String> keywords, int id, String iconName, String des, String cateName) {
        this.status = status;
        this.keywords = keywords;
        this.id = id;
        this.iconName = iconName;
        this.des = des;
        this.cateName = cateName;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }
}
