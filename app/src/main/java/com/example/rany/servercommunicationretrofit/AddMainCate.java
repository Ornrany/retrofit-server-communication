package com.example.rany.servercommunicationretrofit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.rany.servercommunicationretrofit.api_service.KnongDaiService;
import com.example.rany.servercommunicationretrofit.request.MainCateForm;
import com.example.rany.servercommunicationretrofit.response.UploadImageResponse;
import com.example.rany.servercommunicationretrofit.response.create_maincate.CreateMainCateResponse;
import com.example.rany.servercommunicationretrofit.response.create_maincate.Maincate;

import java.io.File;
import java.util.Arrays;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddMainCate extends AppCompatActivity {

    private Button submit;
    private ImageView icon;
    private EditText name, description;
    private Retrofit retrofit;
    private KnongDaiService service;
    public static final int OPEN_GALLERY = 99;
    private String pathImage;
    public static final String TAG = "ooooo";
    private String imageString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_main_cate);

        initView();
        setUpRetrofit();
        initEvent();
    }

    private void initEvent() {
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("oooo", "onClick: ");
                if(ContextCompat.checkSelfPermission(AddMainCate.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        getPackageManager().PERMISSION_GRANTED){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]
                                        {Manifest.permission.READ_EXTERNAL_STORAGE},
                                        OPEN_GALLERY);
                    }
                }
                else
                    openGallery();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createMainCate();
            }
        });

    }

    private void uploadImage(){
        File file = new File(pathImage);
        RequestBody requestBody = RequestBody.create(
                        MediaType.parse("*/*"), file);
        MultipartBody.Part photo =
                MultipartBody.Part.createFormData(
                        "file", file.getName(), requestBody);
        Call<UploadImageResponse> call = service.getImageString(photo);
        call.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                // parse image string to pathImage variable
                imageString = response.body().getData();
                Log.e(TAG, "path "+ imageString );
            }
            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                Log.e("oooo", "onFailure: "+ t.getMessage());
            }
        });
    }

    private void createMainCate() {
        Call<CreateMainCateResponse> call = service.insertNewCategory(
                new MainCateForm(
                        true,
                        Arrays.asList("string"),
                        0,
                        imageString,
                        description.getText().toString(),
                        name.getText().toString()
                )
        );
        call.enqueue(new Callback<CreateMainCateResponse>() {
            @Override
            public void onResponse(Call<CreateMainCateResponse> call, Response<CreateMainCateResponse> response) {
                Maincate maincate = response.body().getMaincate();
                Log.e(TAG, "Create RS: "+ maincate.getCateName());
            }

            @Override
            public void onFailure(Call<CreateMainCateResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ t.getMessage() );
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == OPEN_GALLERY){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openGallery();
            }
        }
    }

    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK,
                Uri.parse(MediaStore.Images.Media.DATA));
        i.setType("image/*");
        startActivityForResult(i, OPEN_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == OPEN_GALLERY){
            if(resultCode == RESULT_OK){
                Uri uri = data.getData();
                // show choose image to user
                icon.setImageURI(uri);
                String[] column = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver()
                                .query(uri, column, null, null, null);
                cursor.moveToFirst();
                pathImage = cursor.getString(cursor.getColumnIndex(column[0]));
                cursor.close();
                // Upload image to server
                uploadImage();
                }
        }
    }

    private void setUpRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://110.74.194.125:15000/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(KnongDaiService.class);
    }

    private void initView() {
        name = findViewById(R.id.edName);
        description = findViewById(R.id.edDescription);
        submit = findViewById(R.id.btnSubmit);
        icon = findViewById(R.id.imvIcon);
    }
}
